/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmbi.algorithm.association;

import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import dmbi.algorithm.support.Combinatorics;

/**
 *
 * @author Tezla
 */
public class AprioriAlgorithm {

    /**
     * Apriori Implementation
     * @param input
     * @param minSup
     * @return
     */
    public static Map<Set<String>, Integer> mine_association(List<Set<String>> input, int minSup){
        Map<Set<String>, Integer> ret = new HashMap<>();
        Combinatorics comb = new Combinatorics();
        Set<String> allItems = new HashSet<>();
        Set<Set<String>> restrictions = new HashSet<>();
        Map<Set<String>, Integer> currentStage = new HashMap<>();
        int maxItera = 0;
        
        for(Set<String> currentTransaction: input){
            allItems.addAll(currentTransaction);
        }
        
        maxItera = allItems.size();
        
        for(int it = 1; it <= maxItera; it++){
            //clears current stage
            currentStage.clear();
            
            for(Set<String> currentTransaction: input){
                //loop to add candidate items
                Set<Set<String>> combinations = comb.make_rcombinations(currentTransaction, restrictions, it);
                for(Set<String> i: combinations){
                    //avoid replacing old values
                    if(!currentStage.containsKey(i)){
                        currentStage.put(i, 0);
                    }
                }
            }
            
            if(currentStage.isEmpty()){
                //just break escape, forget something ever happened!
                break;
            }
            
            for(Set<String> currentTransaction: input){
                //loop to calculate occurences support points
                for(Set<String> currentPair: currentStage.keySet()){
                    if(currentTransaction.containsAll(currentPair)){
                        currentStage.put(currentPair, (currentStage.get(currentPair) + 1));
                    }
                }
            }
            
            //removes all keys with support less than minSup
            List<Set<String>> keys = new LinkedList<>();
            keys.addAll(currentStage.keySet());
            
            for(Set<String> b: keys){
                if(currentStage.get(b) < minSup){
                    restrictions.add(b);
                    currentStage.remove(b);
                }
            }
            
            
            //put all current stage result to return value
            ret.putAll(currentStage);
        }
        
        return ret;
    }
    
}
