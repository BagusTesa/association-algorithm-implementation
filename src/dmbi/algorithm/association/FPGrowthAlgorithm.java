/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmbi.algorithm.association;

import java.util.Map;
import java.util.Set;
import java.util.List;
import java.util.HashSet;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Collections;

/**
 *
 * @author Tezla
 */
public class FPGrowthAlgorithm {
    
    private static class FPTreeNode<FPTreeNode>{
        private String key;
        private int passage;
        private List<FPTreeNode> children;
        
        public FPTreeNode(String k){
            this.key = k;
            this.passage = 0;
            this.children = new LinkedList<>();
        }

        /**
         * @return the key
         */
        public String getKey() {
            return key;
        }

        /**
         * @return the children
         */
        public List<FPTreeNode> getChilds() {
            return getChildren();
        }
        
        public int getPassage(){
            return this.passage;
        }
        
        public synchronized void incrementPassage(){
            this.passage += 1;
        }
        
        public synchronized void setPassage(int passage){
            this.passage = passage;
        }
        
        /**
         * @return the children
         */
        public List<FPTreeNode> getChildren() {
            return children;
        }
        
        @Override
        public String toString(){
            String out;
            out = "<" + this.key + ":" + this.passage + ">";
            if(!this.children.isEmpty()){
                out += this.children;
            }
            return out;
        }
        
    }
    
    private static class FPItemAbst implements Comparable<FPItemAbst>{
        private String key;
        private int support;
        
        public FPItemAbst(String k, int supp){
            this.key = k;
            this.support = supp;
        }
        
        public String getKey(){
            return this.key;
        }
        
        public int getSupport(){
            return this.support;
        }
        
        @Override
        public int compareTo(FPItemAbst o) {
            if(this.support < o.getSupport()){
                return -1;
            }
            else if(this.support > o.getSupport()){
                return 1;
            }
            return 0;
        }
        
        @Override
        public String toString(){
            return this.key + ":" + this.support;
        }
    }
    
    private static List<FPTreeNode> addTransaction(List<FPTreeNode> root, List<FPItemAbst> itemSet){
        List<FPTreeNode> cRoot;
        //initially cRoot points the same list - root, then it
        cRoot = root;
        
        for(FPItemAbst cSeek: itemSet){
            FPTreeNode tmp = null;
            //moves deeper
            for(FPTreeNode in: cRoot){
                if(in.getKey().equals(cSeek.key)){
                    in.incrementPassage();
                    tmp = in;
                    break;
                }
            }
            
            if(tmp == null){
                tmp = new FPTreeNode(cSeek.key);
                tmp.incrementPassage();
                cRoot.add(tmp);
            }

            cRoot = tmp.getChildren();
        }
        //keep pointing to the root
        return root;
    }
    
    private static List<FPTreeNode> buildConditionalPatternBase(List<FPTreeNode> root, String key){
        List<FPTreeNode> ret = new LinkedList<>();
        List<FPTreeNode> recursion;
        FPTreeNode nNode;
        int pSum;
        
        for(FPTreeNode cSeek: root){
            if(cSeek.getKey().compareTo(key) == 0){
                //if it hit the key
                nNode = new FPTreeNode(cSeek.key);
                nNode.setPassage(cSeek.passage);
                ret.add(nNode);
            }
            else{
                //dig down to the leaf
                recursion = buildConditionalPatternBase(cSeek.getChildren(), key);
                if(!recursion.isEmpty()){
                    //if it's not empty (encounters any of the key keeper)
                    nNode = new FPTreeNode(cSeek.key);
                    pSum = 0;
                    for(FPTreeNode uSeek: recursion){
                        pSum += uSeek.getPassage();
                    }
                    nNode.setPassage(pSum);
                    nNode.getChildren().addAll(recursion);
                    ret.add(nNode);
                }
            }
        }
        
        return ret;
    }
    
    private static Map<Set<String>, Integer> mine_fptree(List<FPTreeNode> InputNode, String key){
        Map<Set<String>, Integer> ret = new HashMap<>();
        Map<Set<String>, Integer> rcursion;
        Set<Set<String>> seeker1;
        Set<Set<String>> seeker2;
        Set<String> combinations;
        
        //recursively build pattern from the bottom, merge if recursion had same result
        for(FPTreeNode cSeek: InputNode){
            if(cSeek.getKey().equals(key)){
                combinations = new HashSet<>();
                combinations.add(key);
                ret.put(combinations, (ret.getOrDefault(combinations, 0) + cSeek.getPassage()));
            }
            else{
                rcursion = mine_fptree(cSeek.getChildren(), key);
                //rcursion always has at least a single item, which is had key = key
                
                //make a bigger pattern append to rcursion
                seeker1 = new HashSet<>();
                seeker1.addAll(rcursion.keySet());
                for(Set<String> iSeek: seeker1){
                    combinations = new HashSet<>();
                    combinations.addAll(iSeek);
                    combinations.add(cSeek.getKey());
                    rcursion.put(combinations, rcursion.get(iSeek));
                }
                
                //does ret had rcursion's pattern, if yes, merge.
                seeker1 = new HashSet<>();
                seeker1.addAll(rcursion.keySet());
                for(Set<String> pattern: seeker1){
                    //if ret already had the pattern (item set)
                    if(ret.containsKey(pattern)){
                        //merge
                        ret.put(pattern, ret.get(pattern) + rcursion.get(pattern));
                    }
                    else{
                        //insert as new instance
                        ret.put(pattern, rcursion.get(pattern));
                    }
                }
            }
        }
        
        return ret;
    }
    
    /**
     * FPGrowth Implementation
     * @param input
     * @param minSup
     * @return
     */
    public static Map<Set<String>, Integer> mine_association(List<Set<String>> input, int minSup){
        Map<Set<String>, Integer> ret = new HashMap<>();
        Map<String, Integer> frequentOneItemSet = new HashMap<>();
        List<List<FPItemAbst>> nInput = new LinkedList<>();
        List<FPTreeNode> FPGroot = new LinkedList<>();
        Map<String, List<FPTreeNode>> conditionalPatternBase = new HashMap<>();
        Map<Set<String>, Integer> _ret;
        
        //count all support of singular item instance
        for(Set<String> transactionItems: input){
            for(String item: transactionItems){
                frequentOneItemSet.put(item, (frequentOneItemSet.getOrDefault(item, 0) + 1));
            }
        }
         
        //removes item that < minSup
        Set<String> oSeeker = new HashSet<>();
        oSeeker.addAll(frequentOneItemSet.keySet());
       
        for(String key: oSeeker){
            if(frequentOneItemSet.get(key) < minSup){
                frequentOneItemSet.remove(key);
            }
        }
        
        //convert all item to FPItemAbst instance, so can be sorted - watch your heap level
        for(Set<String> transactionItems: input){
            List<FPItemAbst> tmp = new LinkedList<>();
            for(String item: transactionItems){
                if(!(frequentOneItemSet.getOrDefault(item, 0) < minSup)){
                 tmp.add(new FPItemAbst(item, frequentOneItemSet.get(item)));   
                }
            }
            nInput.add(tmp);
        }
        
        //sort each transactions based on supports
        for(List<FPItemAbst> item: nInput){
            Collections.sort(item);
            Collections.reverse(item);
        }
        
        //create fp-tree
        for(List<FPItemAbst> item: nInput){
            addTransaction(FPGroot, item);
        }
        
        //conditional pattern base | actually this code jumps to make fp-tree directly, cursive! (need to be confirmed)
        for(String key: frequentOneItemSet.keySet()){
            conditionalPatternBase.put(key, buildConditionalPatternBase(FPGroot, key));
        }
        
        //extract pattern from FP-Tree conditionalPatternBase and inject to ret
        for(String key: conditionalPatternBase.keySet()){
            _ret = mine_fptree(conditionalPatternBase.get(key), key);
            for(Set<String> itemSet: _ret.keySet()){
                if(!(_ret.get(itemSet) < minSup)){
                    ret.put(itemSet, _ret.get(itemSet));
                }
            }
        }
        
        return ret;
    }
    
}
