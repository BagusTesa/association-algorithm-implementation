/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmbi.algorithm.support;

import java.util.Set;
import java.util.List;
import java.util.HashSet;
import java.util.LinkedList;

/**
 *
 * @author Tezla
 */
public class Combinatorics {
    private Set<Set<String>> storage;
    private Set<Set<String>> restrictions;
    
    private synchronized void init(){
        storage = new HashSet<>();
        restrictions = new HashSet<>();
    }
    
    public synchronized Set<Set<String>> make_combinations(Set<String> input, int size){
        this.init();
        
        List<String> reference = new LinkedList<>();
        reference.addAll(input);
        
        combine(null, reference, size);
        
        
        return storage;
    }
    
    public synchronized Set<Set<String>> make_rcombinations(Set<String> input, Set<Set<String>> restrictions, int size){
        this.init();
        
        if(restrictions != null){
            this.restrictions.addAll(restrictions);
        }
        
        List<String> reference = new LinkedList<>();
        reference.addAll(input);
        
        combine(null, reference, size);
        
        return storage;
    }
    
    private synchronized void putStorage(List<String> input){
        if(this.storage != null){
            Set tmp = new HashSet<>();
                tmp.addAll(input);
            this.storage.add(tmp);
        }
    }
    
    private boolean isRestricted(List<String> newPrefix){
        //apply restrictions
        for(Set<String> rest: this.restrictions){
            if(newPrefix.containsAll(rest)){
                //it's subset within black-listed list, not worth to list its super set
                return true;
            }
        }
        return false;
    }
    
    private void combine(List<String> prefix, List<String> reference, int size){
        if((prefix != null) && (prefix.size() == size)){
            this.putStorage(prefix);
            return;
        }
        for(int itera = 0; itera < reference.size(); itera++){
            //not memory effective - watch your heap level!
            List<String> newPrefix = new LinkedList<>();
            List<String> newRef = new LinkedList<>();
            
            if(prefix != null){
                newPrefix.addAll(prefix);
            }
            
            //add next item to item set
            newPrefix.add(reference.get(itera));
            
            //check whether items listed in restrictions or not
            if(!isRestricted(newPrefix)){
                //remove all used items
                newRef.addAll(reference);
                   newRef.removeAll(newPrefix);
            
                //recurse, capture next set of combinations
                combine(newPrefix, newRef, size);
            }
            
        }
    }
}
