/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmbi.data.io;

import java.nio.charset.StandardCharsets;
import java.io.IOException;
import java.nio.file.Paths;
import java.nio.file.Path;
import java.nio.file.Files;
import java.util.Set;
import java.util.List;
import java.util.Map;
import java.util.HashSet;
import java.util.LinkedList;
import java.io.BufferedReader;
import java.io.BufferedWriter;


/**
 *
 * @author Tezla
 */
public class CommaSeparatedFile {

    public static List<Set<String>> read(String filepath){
        return read(filepath, ";");
    }
    public static List<Set<String>> read(String filepath, String separatorRegex){
        Path fdir = Paths.get(filepath);
        List<Set<String>> ret = new LinkedList<>();
        String tmp_line;
        String tmp[];
        Set<String> tmpset;
        try(BufferedReader reader = Files.newBufferedReader(fdir, StandardCharsets.UTF_8)) {
            while((tmp_line = reader.readLine()) != null){
                tmpset = new HashSet<>();
                tmp = tmp_line.split(separatorRegex);
                
                for(String ctmp: tmp){
                    //guards empty strings
                    if(ctmp.length() > 0){
                        tmpset.add(ctmp);
                    }
                }
                
                ret.add(tmpset);
            }
        }
        catch(IOException e){
            System.out.println("Whoops! Something were wrong.");
        }
        return ret;
    }
    
    private static int findLongest(Set<Set<String>> reference){
        int ret = 0;
        
        for(Set<String> cref: reference){
            if(cref.size() > ret){
                ret = cref.size();
            }
        }
        
        return ret;
    }
    
    public static void write(String filepath, Map<Set<String>, Integer> dataSource){
        write(filepath, ";", dataSource);
    }
    public static void write(String filepath, String separator, Map<Set<String>, Integer> dataSource){
        Path fdir = Paths.get(filepath);
        String out;
        List<String> items;
        int len = findLongest(dataSource.keySet());
        int clen;
        try(BufferedWriter writer = Files.newBufferedWriter(fdir, StandardCharsets.UTF_8)) {
            for(Set<String> transactionItems: dataSource.keySet()){
                out = "";
                items = new LinkedList<>();
                items.addAll(transactionItems);
                
                clen = items.size();
                for(int itera = 0; itera < len; itera++){
                    if(itera < clen){
                        out += items.get(itera);
                    }
                    out += separator;
                }
                out += dataSource.get(transactionItems);
                writer.write(out);
                writer.newLine();
            }
        }
        catch(IOException e){
            System.out.println("Whoops! something were wrong.");
        }
    }
}
