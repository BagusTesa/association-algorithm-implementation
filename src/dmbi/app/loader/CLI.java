/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmbi.app.loader;

import java.util.Iterator;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.Map;


import dmbi.algorithm.association.AprioriAlgorithm;
import dmbi.algorithm.association.FPGrowthAlgorithm;
import dmbi.data.io.CommaSeparatedFile;

/**
 *
 * @author Tezla
 */
public class CLI {
    
    public static void printhelp(){
        System.out.println("use: java -jar this.jar -a algorithm -f fileInput -s inputItemSeparator -m minSup -o fileOutput");
        System.out.println("with:");
        System.out.println("\talgorithm apriori or fpg");
        System.out.println("\tminSup in integer, the minimum number of transaction to consider a pattern worthy");
        System.out.println("\tSeparator can be any characters but it had make sense (to you and to the application) by default we used ; as separator");
        System.out.println("\tps. make sure there is no space between item and separator, it'll make problem(s) later as this application blindly splits string by separator");
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //java -jar this.jar -a algorithm -f fileInput -s inputItemSeparator -m minSup -o fileOutput
        List<String> params = Arrays.asList(args);
        Iterator<String> it = params.iterator();
        
        /**Configuration Table**/
        String algo = null;
        String fileInput = null;
        String separator = null;
        int minSup = -1;
        String fileOutput = null;
        /**/
        
        
        String cparm = null;
        String cargs = null;
        
        if(params.isEmpty()){
            printhelp();
            System.out.println("No arguments passed");
            return;
        }
        
        while((it.hasNext()) && ((cparm = it.next()) != null)){
            cparm = cparm.toLowerCase();
            if(it.hasNext()){
                //ensure this throw error
                cargs = it.next().toLowerCase();
                
                if(cparm.equals("-a")){
                    algo = cargs;
                }
                
                else if(cparm.equals("-f")){
                    fileInput = cargs;
                }
                else if(cparm.equals("-s")){
                    separator = cargs;
                }
                else if(cparm.equals("-m")){
                    minSup = Integer.decode(cargs);
                }
                else if(cparm.equals("-o")){
                    fileOutput = cargs;
                }
                else{
                    System.out.println("Unrecognized arguments");
                    printhelp();
                    return;
                }
            }
            else{
                System.out.println("Missing arguments near: " + cparm);
                printhelp();
                return;
            }
        }
        
        if(algo == null){
            System.out.println("-a argument is mandatory");
            return;
        }
        
        if(fileInput == null){
            System.out.println("-f argument is mandatory");
            return;
        }
        
        if(minSup < 0){
            System.out.println("minimum support should be >0, define with -m");
            return;
        }
        
        //do the job - you have only one job!
        List<Set<String>> dataSauce = null;
        Map<Set<String>, Integer> output = null;
        if(separator != null){
            dataSauce = CommaSeparatedFile.read(fileInput, separator);
        }
        else{
            dataSauce = CommaSeparatedFile.read(fileInput);
        }
        
        if(dataSauce == null){
            //something actually wrong but not flagged as wrong - nobody throws exception
            System.out.println("Screw this!");
            return;
        }
        
        if(algo.equalsIgnoreCase("apriori")){
            output = AprioriAlgorithm.mine_association(dataSauce, minSup);
        }
        else if(algo.equalsIgnoreCase("fpg")){
            output = FPGrowthAlgorithm.mine_association(dataSauce, minSup);
        }
        
        if(output == null){
            //something actually wrong but none raised exception
            System.out.println("Driver this!");
            return;
        }
        
        if(fileOutput != null){
            if(separator != null){
                CommaSeparatedFile.write(fileOutput, separator, output);
            }
            else{
                CommaSeparatedFile.write(fileOutput, output);
            }
        }
        else{
            System.out.println("");
            System.out.println(output);
            System.out.println("");
        }
        
        System.out.println("done~");
    }
    
}
