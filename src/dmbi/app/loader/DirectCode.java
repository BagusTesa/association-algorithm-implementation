/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dmbi.app.loader;

import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.LinkedList;
import dmbi.algorithm.association.AprioriAlgorithm;
import dmbi.algorithm.association.FPGrowthAlgorithm;
import dmbi.data.io.CommaSeparatedFile;

/**
 *
 * @author Tezla
 */
public class DirectCode {

    public static List<Set<String>> createExample(){
        List<Set<String>> abctransaction = new LinkedList<>();
        
        Set<String> tmp;
        
        tmp = new HashSet<>();
            tmp.add("Figma");
            tmp.add("Gundam");
        abctransaction.add(tmp);
        tmp = new HashSet<>();
            tmp.add("Gundam");
            tmp.add("Nendoroid");
        abctransaction.add(tmp);
        tmp = new HashSet<>();
            tmp.add("Nendoroid");
            tmp.add("Figma");
            tmp.add("Dakimakura");
        abctransaction.add(tmp);
        tmp = new HashSet<>();
            tmp.add("Figma");
            tmp.add("Dakimakura");
            tmp.add("Nendoroid");
        abctransaction.add(tmp);
        tmp = new HashSet<>();
            tmp.add("Gundam");
            tmp.add("Zoid");
            tmp.add("Figma");
        abctransaction.add(tmp);
        tmp = new HashSet<>();
            tmp.add("Zoid");
            tmp.add("Figma");
        abctransaction.add(tmp);
        tmp = new HashSet<>();
            tmp.add("Figma");
            tmp.add("Gundam");
        abctransaction.add(tmp);
        tmp = new HashSet<>();
            tmp.add("Nendoroid");
            tmp.add("Dakimakura");
        abctransaction.add(tmp);
        
        return abctransaction;
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //code here if you want to toy with this
        
    }
    
}
