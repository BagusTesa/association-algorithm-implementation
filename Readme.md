Created solely to do [this task](http://elisa.ugm.ac.id/community/show/data-mining-dan-business-intelligence-kelas-b/).

**Requirements:**

[Java Development Kit 8 Update 66](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

[Netbeans 8.1](https://netbeans.org/downloads/)

**Notice:**

As this project uses JDK 8, it extensively uses [JDK 8 features](http://www.oracle.com/technetwork/java/javase/8-whats-new-2157071.html):

* Collection (List, Set, Map, and their implementation - LinkedList, HashSet, HashMap)

```
#!Java
//in FPGrowthAlgorithm Line 111
Collections.sort(item);
Collections.reverse(item);
```

* Improved Type Inference

```
#!Java
//in AprioriAlgorithm line 29
Map<Set<String>, Integer> ret = new HashMap<>();
```

So, if you wish to port this project to earlier JDK, you have to change several line of codes to comply with earlier JDK. Also i can guarantee that these code isn't efficient (yet) as it's still a mock up.

**How To Use:**

It's kind of obvious, but here's our captain obvious. You had to compile (**clean build**) this project first (it includes no jars, only `.java`, `.class`, and several other files like the one you're reading right now). After being compiled, you can execute the jar with these parameter:

* -a algorithm_name

     Supported algorithm_name are: **apriori**, **fgp**. We'd be interested to put new algorithm implementation. But alas! We can't muster the motivation, support, dedication, and even we're still struggling to get graduated.

* -f input_file

     Input file is path - relative or not (preferably absolute path) - to input file with csv-like headless (without header) format.
     Examples are stored in `/example` directory.

* -m minimum_support

     [Minimum support](https://en.wikipedia.org/wiki/Association_rule_learning#Support) is the number of transaction (lines in file) to consider a combination of object is an important pattern. In our case, we don't bother with calculating it relative to total transactions - we accept only integers (ie. minimum_support = 5 regardless the number of all transactions), this simplifies several calculation inside the implementation and actually [avoids floating point problems](https://docs.oracle.com/cd/E19957-01/806-3568/ncg_goldberg.html). Although we hope to have this support somewhere in the next several *days* - if we can pile up the motivation.

* -o output_file*

      Output file is path - relative or not (preferably absolute path). This parameter will set the program to dump it's output (association table, not the actual rule) as the homework strictly said that way - and we won't bother it unless we *can pile up the motivation*. This parameter is unnecessary unless you wish to dump the output to file instead of your puny command line window.

To run the program, which kind of obvious (again) done through:

      `java -jar this_jar_name.jar -a algorithm_name -f input_file -m minimum_support`

And Finally, our *motto*:

      *`Not now please. Right now, i have my own problem.`*
